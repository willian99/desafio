from django.db import models

# Create your models here.

from desafio_app.models import projeto


class atividade(models.Model):

    ID_Projeto = models.ForeignKey(projeto)
    Nome_Atividade = models.CharField(max_length=200,unique=True)
    Data_Inicial = models.DateField()
    Data_Final = models.DateField()
    Finalizado = models.BooleanField(default=False)
    Ativo_Atividade = models.BooleanField(default=True)


    def __str__(self):
        return  self.Nome_Atividade