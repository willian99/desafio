from django.db import models

# Create your models here.

class projeto(models.Model):

    Nome_Projeto  = models.CharField(max_length=200,unique=True)
    Data_Inicial  = models.DateField()
    Data_Final    = models.DateField()
    Ativo         = models.BooleanField(default=True)


    def __str__(self):
        return   self.Nome_Projeto