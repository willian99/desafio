#coding: utf-8
from django.views.generic.base import View
from django.shortcuts import render

from desafio_app.models import projeto


class Index(View):

    def get(self, request):

        pro  = projeto.objects.all()
        context_dict = {}
        return render(request, 'Index.html', {'pro':pro})


