//  @@@@ Chamando objetos @@@@

//  Documento: $(document)
//  HTML: $('div');
//  ID: $('#batata');
//  CLASSE: $('.clue');

// @@@@ Funções @@@@

// ready: quando o objeto estiver totalmente carregado no navegador
// click: quando clicar
// onfocus: quando um campo "input" é acionado

$(document).ready(function () {



    $('.ui.accordion').accordion();
    $('.ui.dropdown').dropdown();


    if ($('#serviceMessenger').val() !== "") {
        $('.ui.modal').modal('show')
    };

    $('.ui.radio.checkbox').checkbox();




});







