#coding: utf-8
from django.shortcuts import render,HttpResponseRedirect
from django.views.generic.base import View

from  desafio_app.models import projeto


class lista_excluir_projeto(View):

    template = "Lista_Excluir_Projeto.html"


    def get(self, request):
        lista = projeto.objects.filter(Ativo=True)
        return render(request, self.template, {'lista': lista})