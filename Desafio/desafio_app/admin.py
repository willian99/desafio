#coding: utf-8
from django.contrib import admin

from desafio_app.models import projeto
from desafio_app.models import atividade

admin.site.register(projeto)
admin.site.register(atividade)

