#coding: utf-8
from django.conf.urls import url


from desafio_app.views.Index import Index
from desafio_app.views.Cadastro_Projeto import cadastro_projeto

from desafio_app.views.Cadastro_Atividades import cadastro_atividades
from desafio_app.views.Andamento_Projeto import andamento_projeto
from desafio_app.views.Andamento_Projeto_Resultado import andamento_projeto_resultado

from desafio_app.views.Status_Projeto import status_projeto
from desafio_app.views.Status_Projeto_Resultado import status_projeto_resultado

from desafio_app.views.Desativar_Projeto import desativar_projeto

from desafio_app.views.Lista_Excluir_Projeto import lista_excluir_projeto

from desafio_app.views.Lista_Excluir_Atividade import lista_excluir_atividade

from desafio_app.views.Desativar_Atividade import desativar_atividade

urlpatterns = [
    url(r'^index/$', Index.as_view(), name='index'),
    url(r'^cadastrar_projeto/$', cadastro_projeto.as_view(), name='Cadastrar_Projeto'),
    url(r'^cadastrar_atividade/$', cadastro_atividades.as_view(), name='Cadastrar_Atividade'),
    url(r'^andamento_projeto/$', andamento_projeto.as_view(), name='Andamento_Projeto'),
    url(r'^status_projeto/$', status_projeto.as_view(), name='Status_Projeto'),
    url(r'^excluir_projeto/$', lista_excluir_projeto.as_view(), name='Lista_Excluir_Projeto'),
    url(r'^lista_excluir_atividade/$', lista_excluir_atividade.as_view(), name='Lista_Excluir_Atividade'),
    url(r'^andamento_projeto_resultado/(?P<id>\d+)/$', andamento_projeto_resultado .as_view(), name='Andamento_Projeto_Resultado'),
    url(r'^status_projeto_resultado/(?P<id>\d+)/$', status_projeto_resultado .as_view(), name='Status_Projeto_Resultado'),
    url(r'^desativar_projeto/(?P<id>\d+)/$', desativar_projeto.as_view(), name='Desativar_Projeto'),
    url(r'^desativar_atividade/(?P<id>\d+)/$', desativar_atividade.as_view(), name='Desativar_Atividade')

]