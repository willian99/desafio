#coding: utf-8
from django.shortcuts import render,HttpResponseRedirect
from django.views.generic.base import View

from  desafio_app.models import atividade


class lista_excluir_atividade(View):

    template = "Lista_Excluir_Atividade.html"


    def get(self, request):
        lista = atividade.objects.filter(Ativo_Atividade=True)
        return render(request, self.template, {'lista': lista})