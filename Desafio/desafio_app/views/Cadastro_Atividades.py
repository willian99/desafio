#coding: utf-8
from django.shortcuts import render,HttpResponseRedirect
from django.views.generic.base import View

from desafio_app.forms.Form_Cadastro_Atividades import Form_Cadastro_Atividades


class cadastro_atividades(View):

    template = "Cadastro_Atividades.html"
    template1 = "Cadastro_Sucesso_Atividades.html"
    conteudo = {'form': Form_Cadastro_Atividades()}

    def post(self, request):
        form = Form_Cadastro_Atividades(request.POST)
        if form.is_valid():
            form.save(commit=True)
            print ("entrou")
            self.conteudo.update({'messengerSucess': 'Cadastrado com Sucesso', 'messengerError': ''})
            mensagem = "Cadastrado com sucesso!"
            return render(request, self.template1, {'mensagem':mensagem})
        else:
            print(form.errors)
            return render(request, self.template, {'form': form})
        return render(request, self.template, self.conteudo)

    def get(self, request):
        form = Form_Cadastro_Atividades()
        return render(request, self.template, {'form': form})