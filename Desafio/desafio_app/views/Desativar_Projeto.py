#coding: utf-8

from django.views.generic.base import View
from django.shortcuts import render,HttpResponseRedirect

from desafio_app.models import projeto

from desafio_app.forms.Forms_para_Desativar import Form_Desativar_Projeto


class desativar_projeto(View):
    template = "Excluido_Projeto.html"

    conteudo = {
        'editar': Form_Desativar_Projeto(),
    }


    def get(self, request, id=None):
        if id:

            dependente = projeto.objects.get(pk=id)
            form = Form_Desativar_Projeto(instance=dependente, data=request.POST)
        else:
            id = None
            form = Form_Desativar_Projeto(data=request.POST)

        if form.is_valid():
            form.save()
            self.template = "Excluido_Projetos.html"
            mensagem = 'Eliminado com Sucesso'
            print('Foi excluido')
            return render(request,self.template,{'mensagem':mensagem }) # arrumar aqui
        else:
            print("Nao excluiu")
            print(form.errors)

        return render(request, self.template, {'form': form, 'method': 'post', 'id': id})




    def post(self, request,id=None):
        if request.POST['id']:
            id = request.POST['id']
            dependente = projeto.objects.get(pk=id)
            form =  Form_Desativar_Projeto(instance=dependente, data=request.POST)
        else:
            id = None
            form = Form_Desativar_Projeto(data=request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/Desafio/status_projeto/' + id ) # arrumar aqui
        else:
            print(form.errors)

        return render(request, self.template, {'form': form, 'method': 'post', 'id': id})
