from django import forms

from desafio_app.models import projeto


class Form_Cadastro_Projeto(forms.ModelForm): # uma funcao com o nome PostForm

    class Meta:
        model = projeto
        fields = ('Nome_Projeto','Data_Inicial','Data_Final')

