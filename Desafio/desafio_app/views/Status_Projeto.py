#coding: utf-8
from django.shortcuts import render,HttpResponseRedirect
from django.views.generic.base import View

from  desafio_app.models import projeto


class status_projeto(View):

    template = "Status_Projeto.html"


    def get(self, request):
        lista = projeto.objects.filter(Ativo=True)
        return render(request, self.template, {'lista': lista})