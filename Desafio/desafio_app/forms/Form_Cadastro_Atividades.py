from django import forms

from desafio_app.models import atividade


class Form_Cadastro_Atividades(forms.ModelForm): # uma funcao com o nome PostForm

    class Meta:
        model = atividade
        fields = ('ID_Projeto','Nome_Atividade', 'Data_Inicial', 'Data_Final','Finalizado')
