#coding: utf-8
from django.views.generic.base import View
from django.shortcuts import render

from  desafio_app.models import projeto
from  desafio_app.models import atividade


class status_projeto_resultado(View):

    template = "Status_Projeto_Resultado.html"

    def get(self, request,id):
        pro = projeto.objects.get(id=id)
        ati = atividade.objects.filter(ID_Projeto = id)


        data_pro = pro.Data_Final

        var = []
        var3 = []
        sem_valor = 2

        if(len(ati) > 0):
            for x in ati:
                data = x.Data_Final
                var.append(data)

                if(data > data_pro ):
                    print('Projeto vai atrasar por causa da atividade', x.Nome_Atividade)
                    if(x.Ativo_Atividade == True):
                        var3.append(x)

                if(len(var3) >= 1):
                    sem_valor = 1

        return render(request, self.template, {'var3': var3,'data_pro':data_pro,'sem_valor':sem_valor})
