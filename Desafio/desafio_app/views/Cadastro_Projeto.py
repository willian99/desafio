#coding: utf-8
from django.shortcuts import render,HttpResponseRedirect
from django.views.generic.base import View

from desafio_app.forms.Form_Cadastro_Projeto import Form_Cadastro_Projeto


class cadastro_projeto(View):

    template = "Cadastro_Projeto.html"
    template1 = "Cadastro_Sucesso_Projeto.html"
    conteudo = {'form': Form_Cadastro_Projeto()}

    def post(self, request):

        form = Form_Cadastro_Projeto(request.POST)
        if form.is_valid():
            form.save(commit=True)
            print ("entrou")
            self.conteudo.update({'messengerSucess': 'Cadastrado com Sucesso', 'messengerError': ''})
            mensagem = "Cadastrado com sucesso!"

            return render(request, self.template1, {'mensagem':mensagem})
        else:
            print(form.errors)
            return render(request, self.template, {'form': form})
        return render(request, self.template, self.conteudo)

    def get(self, request):
        form = Form_Cadastro_Projeto()
        return render(request, self.template, {'form': form})