#coding: utf-8
from django.views.generic.base import View
from django.shortcuts import render

from  desafio_app.models import projeto
from  desafio_app.models import atividade




class andamento_projeto_resultado(View):

    template = "Andamento_Projeto_Resultado.html"

    def get(self, request,id):
        pro = projeto.objects.get(id=id)
        ati = atividade.objects.filter(ID_Projeto = id)

        counter = 0
        for c in ati:
            counter += 1

        not_finalizado = 0
        yes_finalizado = 0

        for x in ati:
            if(x.Finalizado == False):
                not_finalizado +=1
            else:
                yes_finalizado +=1

        if(yes_finalizado >= 1):
            lista = []
            total =  (yes_finalizado * 100) / counter
            lista = total
            print(lista)

            return render(request, self.template, {'lista': lista})
        else:
            context_dict = 0
            return render(request, self.template, {'context_dict':context_dict})