Tecnologias Utilizadas

Framework: Django 1.11.6.

Backend
Linguagem de Programação: Python 3.6; Banco de Dados: Sqlite 3.

Frontend
HTML, CSS e Semantc-ui.

Instalações necessárias em ambiente Linux:

1) Verificar se o Python está instalado na máquina, para isso utilizar o comando abaixo: 

python3 --version

1.1) Caso não esteja instalado, executar o comando: 

sudo apt install python3

2) Criar uma pasta e, dentro desta pasta Instalar a Virtualenv: 

cd nome_da_pasta

python3 -m venv myvenv

3) Ativar a Virtualenv: 

source myvenv/bin/activate

4) Instalando o Django (versão 1.11.6):

pip install django==1.11.6

5) Para verificar se a instalação do Django ocorreu com sucesso utilizar o comando:

django-admin --version

Obs: Caso ocorra algum problema após a instalação do Django, utilizar o comando abaixo:
sudo pip install --upgrade django
 

Rodando o Projeto:

Baixar o projeto no repositório do bitbucket;

2) Copiar a pasta Desafio e colar dentro da pasta criada anteriormente;

3) Acessar via terminal a pasta Desafio (lembrando que precisa estar com a virtualenv ativa) e rodar o Projeto:
cd Desafio  

python manage.py runserver

4)O terminal mostrará um endereço de URL, normalmente http://127.0.0.1:8000. Complementar a URL com o nome (/Desafio/index/ ). 
Exemplo: http://127.0.0.1:8009/Desafio/index/



